import { newGame, distributeStones } from './mancala.js';

describe('mancala', () => {
  it('The initial game state', () => {
    var expectedState = {
      player: 0,
      board: [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0],
    };
    var startState = newGame();

    expect(expectedState).toEqual(startState);
  });
  it('Distributing pieces', () => {
    var startState = {
      player: 0,
      board: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    };
    var expectedState = {
      player: 1,
      board: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    };
    var currentState = distributeStones(startState, 0);

    expect(expectedState).toEqual(currentState);
  });
  it('Distributing pieces', () => {
    var startState = {
      player: 0,
      board: [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    };
    var expectedState = {
      player: 1,
      board: [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    };
    var currentState = distributeStones(startState, 0);

    expect(expectedState).toEqual(currentState);
  });
  it('Distributing pieces', () => {
    var startState = {
      player: 0,
      board: [13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    };
    var expectedState = {
      player: 1,
      board: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    };
    var currentState = distributeStones(startState, 0);

    expect(expectedState).toEqual(currentState);
  });
});
