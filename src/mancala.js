// Drivers, Navigator

// Lucas, Ben;
// Ben, Sam;
// Sam, Lucas;

function newGame() {
  let gameObject = {
    player: 0,
    board: [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0],
  };
  return gameObject;
}

function distributeStones(gameObject, hollowPicked) {
  let numberOfStonesToDistribute = gameObject.board[hollowPicked];
  let newBoard = gameObject.board;
  newBoard[hollowPicked] = 0;

  for (let i = hollowPicked + 1; i <= numberOfStonesToDistribute; i++) {
    if (i + numberOfStonesToDistribute >= gameObject.board.length) {
      i = 0; //if i = 13 i % 13???;
      numberOfStonesToDistribute--;
    }
    newBoard[hollowPicked + i] += 1;
  }

  var newState = {
    player: gameObject.player + 1,
    board: newBoard,
  };
  return newState;
}

module.exports = {
  newGame,
  distributeStones,
};
